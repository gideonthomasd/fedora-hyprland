#!/bin/bash

sudo dnf in hyprland xdg-desktop-portal-hyprland yt-dlp remmina remmina-plugins-rdp freerdp NetworkManager-tui waybar blueman vlc pavucontrol lxpolkit xfce4-terminal thunar wlogout rofi geany python3-requests audacious neofetch
sudo dnf copr enable solopasha/hyprland
sudo dnf in swww
sudo dnf copr enable tofik/nwg-shell
sudo dnf in nwg-look
sudo dnf in hyprlock
sudo dnf in cliphist
sudo dnf in eza
 
