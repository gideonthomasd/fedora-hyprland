#!/bin/bash

curl -sS https://starship.rs/install.sh | sh

### Set up geany and neofetch ### 
mkdir -p ~/.config/geany/colorschemes
cd colorschemes
cp -r * ~/.config/geany/colorschemes
cd ..
cp geany.conf ~/.config/geany/geany.conf

mkdir -p ~/.config/neofetch
cp config.conf ~/.config/neofetch/config.conf


### Starship ###
cp starship.toml ~/.config/starship.toml

### Wallpaper ###
#mkdir -p ~/wallpaper
#cd wallpaper
#cp -r * ~/wallpaper
#cd ..

### Themes ###
mkdir -p ~/.icons/Bibata-Modern-Ice
mkdir -p ~/.themes


tar -xzvf kora.tar.gz
tar -xzvf candy-icons.tar.gz
tar -xzvf Dracula.tar.gz
tar -xzvf Sweet-Dark.tar.gz

cp -r kora ~/.icons/kora
cp -r candy-icons ~/.icons/candy-icons
cp -r Dracula ~/.themes/Dracula
cp -r Sweet-Dark ~/.themes/Sweet-Dark

### Music ###
cp Praise.mp3 ~/Music/Praise.mp3

### Cursor ###
cd Bibata-Modern-Ice
cp -r * ~/.icons/Bibata-Modern-Ice
cd ..


cp bashrc ~/.bashrc
#cp Xresources ~/.Xresources

#mkdir -p ~/.local/share/xfce4/terminal/colorschemes
#cp Dracula.theme ~/.local/share/xfce4/terminal/colorschemes/Dracula.theme
